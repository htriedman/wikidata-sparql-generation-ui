#/bin/bash

echo "Stopping webservice..."
webservice stop

echo "Updating source code..."
rm -rf $HOME/www/python/src/*
cp -R $HOME/wikidata-sparql-generation-ui/* $HOME/www/python/src/

if [ "$1" == "reset" ]; then
    echo "Updating environment... (this may take a minute)"
    rm -rf $HOME/www/python/venv
    webservice --backend=kubernetes python3.9 shell -- webservice-python-bootstrap --fresh
    # webservice --backend=kubernetes python3.9 shell
    # webservice --backend=kubernetes python3.9 shell -- python3 -m venv $HOME/www/python/venv && source $HOME/www/python/venv/bin/activate && pip install --upgrade pip wheel && pip install torch --index-url https://download.pytorch.org/whl/cpu && pip install flask flask_httpauth flask_cors annoy sentence_transformers openai python-dotenv tiktoken

    # webservice --backend=kubernetes python3.9 shell -- python3 -m venv $HOME/www/python/venv
    # webservice --backend=kubernetes python3.9 shell -- source $HOME/www/python/venv/bin/activate
    # webservice --backend=kubernetes python3.9 shell -- pip install --upgrade pip wheel
    # webservice --backend=kubernetes python3.9 shell -- pip install torch --index-url https://download.pytorch.org/whl/cpu
    # webservice --backend=kubernetes python3.9 shell -- pip install flask flask_httpauth flask_cors annoy sentence_transformers openai python-dotenv tiktoken
    # python3 -m venv $HOME/www/python/venv
    # source $HOME/www/python/venv/bin/activate
    # pip install --upgrade pip wheel
    # pip install -r $HOME/www/python/src/requirements.txt
fi 

echo "Copying query embedding and dataset to operational location..."
mkdir $HOME/www/python/src/embeddings
cp $HOME/embeddings/* $HOME/www/python/src/embeddings

echo "Copying environment variables to operational location..."
cp $HOME/.env $HOME/www/python/src/.env

echo "Restarting webservice..."
webservice --backend=kubernetes python3.9 start
