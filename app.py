import os
import pickle
import json

from flask import Flask, render_template, request, jsonify
# from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS
import urllib.parse
from annoy import AnnoyIndex  # Search index for embeddings
from sentence_transformers import SentenceTransformer  # Embeddings model
from openai import OpenAI
from dotenv import load_dotenv
import tiktoken
import torch
import mysql.connector

load_dotenv()

# env stuff
client = OpenAI(
  api_key=os.getenv('OPENAI_API_KEY'),
)

app = Flask(__name__)
# auth = HTTPBasicAuth()
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

db_user = os.getenv('DB_USER')
db_pass = os.getenv('DB_PASS')
db = os.getenv('DB')
db_host = 'tools.db.svc.wikimedia.cloud'

torch.set_num_threads(1)

# A very simple check for a hardcoded password
# @auth.verify_password
# def verify_password(username, password):
#     verify = username == os.getenv('FLASK_USERNAME') and password == os.getenv('FLASK_PASSWORD')
#     return verify

# base prompt that query, items and properties will be put into
BASE_PROMPT = '''
Translate the prompt or question below into a SparQL query on Wikidata. Do not use backticks around the code, only return the query itself.
{prompt}

Related Wikidata items: {items}
Related Wikidata properties: {properties}

The following prompt-SparQL query pairs may be useful as context. If they are not relevant, disregard them:
{context}
'''

# system prompt for gpt
SYS_PROMPT = "You are a code translator who translates natural language into SparQL queries."

# https://www.sbert.net/docs/pretrained_models.html#sentence-embedding-models/
# https://huggingface.co/sentence-transformers/all-mpnet-base-v2
EMB_MODEL_NAME = 'all-MiniLM-L6-v2'
EMB_MODEL = SentenceTransformer(f'sentence-transformers/{EMB_MODEL_NAME}')
EMB_NUM_DIMENSIONS = EMB_MODEL.get_sentence_embedding_dimension()

# Embeddings -> Database
# See https://github.com/spotify/annoy for setting these parameters
ANNOY_DIR = 'embeddings'
ANNOY_INDEX = AnnoyIndex(EMB_NUM_DIMENSIONS, 'angular')
INDEX_FN = os.path.join(ANNOY_DIR, f'{EMB_MODEL_NAME}_embeddings.ann')
DS_FN = os.path.join(ANNOY_DIR, 'queries.pkl')
if os.path.exists(INDEX_FN):
    # if return to the notebook to test, don't accidentally overwrite index
    ANNOY_INDEX.load(INDEX_FN)

if os.path.exists(DS_FN):
    with open(DS_FN, 'rb') as f:
        DS = pickle.load(f)

def num_tokens_from_messages(messages, model="gpt-3.5-turbo"):
    """Returns the number of tokens used by a list of messages."""
    app.logger.info("calculating number of tokens for messages")
    try:
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        encoding = tiktoken.get_encoding("cl100k_base")
    if model == "gpt-3.5-turbo":  # note: future models may deviate from this
        num_tokens = 0
        for message in messages:
            num_tokens += 4  # every message follows <im_start>{role/name}\n{content}<im_end>\n
            for key, value in message.items():
                num_tokens += len(encoding.encode(value))
                if key == "name":  # if there's a name, the role is omitted
                    num_tokens += -1  # role is always required and always 1 token
        num_tokens += 2  # every reply is primed with <im_start>assistant
        return num_tokens
    else:
        raise NotImplementedError(f"""num_tokens_from_messages() is not presently implemented for model {model}.
    See https://github.com/openai/openai-python/blob/main/chatml.md for information on how messages are converted to tokens.""")

def search_related_queries(query, ds=DS, result_depth=5):
    """Full pipeline of going from query to result."""
    app.logger.info("searching related queries")
    embedding = EMB_MODEL.encode(query)
    nns = ANNOY_INDEX.get_nns_by_vector(embedding, result_depth, search_k=-1)
    results = []
    for i in range(result_depth):
        try:
            idx = nns[i]
            results.append({
                'prompt': ds['input'][idx],
                'query': ds['output'][idx],
                'url': "https://query.wikidata.org/#" + urllib.parse.quote(ds['output'][idx])
            })
        except:
            break
    return results

def generate_sparql_query(prompt, items, properties, context):
    app.logger.info("generating sparql query")
    context_idx = 2
    messages = [
        {"role": "system", "content": SYS_PROMPT},
        {"role": "user", "content": BASE_PROMPT.format(
            prompt=prompt,
            items=items,
            properties=properties,
            context=[f'prompt {i}: {q["prompt"]}\nquery {i}: {q["query"]}\n\n' for i, q in enumerate(context[:context_idx])]
        )}
    ]
    
    while num_tokens_from_messages(messages) > 4190:
        app.logger.info("allowed context length exceeded")
        context_idx -= 1
        messages[1]['content'] = BASE_PROMPT.format(
            prompt=prompt,
            items=items,
            properties=properties,
            context=[f'prompt {i}: {q["prompt"]}\nquery {i}: {q["query"]}\n\n' for i, q in enumerate(context[:context_idx])]
        )

        if context_idx == 0:
            raise ValueError(f"Prompt or other information is currently too long. Max tokens is 4190; current number of tokens is {num_tokens_from_messages(messages)}")

    completion = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=messages
    )

    app.logger.info("prediction complete")
    
    if completion.choices[0].finish_reason == "length":
        raise ValueError(f"Incomplete generation because prompt + output is too long. Try again with a shorter prompt.")
    elif completion.choices[0].finish_reason == "content_filter":
        raise ValueError(f"Your prompt tripped the OpenAI content filter.")
    elif completion.choices[0].finish_reason == None:
        raise ValueError(f"API response still in progress or incomplete.")
    else:
        return completion.choices[0].message.content
    
@app.route('/save_sparql_query', methods=['POST'])
def save_sparql_query():
    data = request.get_json()
    prompt = data.get('prompt')
    sparql_query = data.get('query')
    
    if not sparql_query:
        return jsonify(success=False, error="No SPARQL query provided.")
    
    if not prompt:
        return jsonify(success=False, error="No prompt provided")

    try:
        # Connect to the MySQL database
        connection = mysql.connector.connect(
            host=db_host,
            user=db_user,
            password=db_pass,
            database=db
        )
        cursor = connection.cursor()

        # Insert the SPARQL query into the database
        query = "INSERT INTO sparql_queries (prompt, query) VALUES (%s, %s)"
        cursor.execute(query, (prompt, sparql_query))
        connection.commit()

        # Close the database connection
        cursor.close()
        connection.close()

        return jsonify(success=True)
    except mysql.connector.Error as err:
        app.logger.error("Error: {}".format(err))
        return jsonify(success=False, error=str(err))


@app.route('/', methods=['GET', 'POST'])
# @auth.login_required
def index():
    if request.method == 'POST':
        button_pressed = request.form['button_pressed']
        prompt = request.form['prompt']
        items = request.form.getlist('qids')
        properties = request.form.getlist('pids')

        try:
            if button_pressed == 'search_corpus':
                if not prompt:  
                    # Handle the case when there's no prompt
                    return render_template('index.html', error="Please enter a prompt.") 

                results = search_related_queries(prompt)
                sparql_query = ""
                query_url = ""
            elif button_pressed == 'generate_query':
                if not prompt:  
                    # Handle the case when there's no prompt, item, or property
                    return render_template('index.html', error="Please enter a prompt. Entering additional items and properties may help with accuracy.") 

                results = ""
                sparql_query = generate_sparql_query(prompt, items, properties, search_related_queries(prompt))
                query_url = "https://query.wikidata.org/#" + urllib.parse.quote(sparql_query)
            elif button_pressed == 'do_both':
                if not prompt:  
                    # Handle the case when there's no prompt, item, or property
                    return render_template('index.html', error="Please enter a prompt. Entering additional items and properties may help with accuracy.") 

                results = search_related_queries(prompt)
                sparql_query = generate_sparql_query(prompt, items, properties, results)
                query_url = "https://query.wikidata.org/#" + urllib.parse.quote(sparql_query)

            return render_template(
                'index.html',
                prompt=prompt,
                items=json.dumps(items),
                properties=json.dumps(properties),
                results=results,
                sparql_query=sparql_query,
                query_url=query_url
            )
        except Exception as e: 
            # Catch any unexpected errors
            return render_template('index.html', error="An error occurred: {}".format(e))  
    else:
        return render_template('index.html')

if __name__ == '__main__':
    # app.run(port=8000, debug=True)
    app.run()